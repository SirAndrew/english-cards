package com.example.englishcards;

import android.content.Context;

import com.example.englishcards.Game.GameController;

import androidx.test.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    Context appContext = InstrumentationRegistry.getTargetContext();
    GameController gameController = new GameController(appContext);

    @Test
    public void DatabaseResponse() {
        assertTrue(gameController.getCardList().size() != 0);
    }

    @Test
    public void CreateQuestionTest() {
        assertEquals(4, gameController.CreateQuestion().size());
    }
}

package com.example.englishcards.Model;

import android.widget.Button;

import static com.example.englishcards.Model.Data.MODE;

public class SimpleCard extends Card {
    public SimpleCard(String rus, String eng) {
        super(rus, eng);
    }

    @Override
    public void fillInterface(Object button) {
        if (MODE == GameMode.ENG_RUS)
            ((Button) button).setText(this.getRus());
        else
            ((Button) button).setText(this.getEng());
    }
}

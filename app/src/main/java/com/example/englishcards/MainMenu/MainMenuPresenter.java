package com.example.englishcards.MainMenu;

import android.content.Context;
import android.content.Intent;

import com.example.englishcards.Game.GameActivity;
import com.example.englishcards.Model.Data;
import com.example.englishcards.Model.GameMode;
import com.example.englishcards.Stats.StatsActivity;

public class MainMenuPresenter implements MainMenuContract.Presenter {

    private MainMenuContract.View view;
    private MainMenuContract.Repository repository;
    private Context context;


    public MainMenuPresenter(MainMenuContract.View view, Context context) {
        this.view = view;
        repository = new MainMenuRepository();
        this.context = context;
    }

    @Override
    public void startGame(GameMode gameMode) {
        Data.MODE = gameMode;
        Intent intent = new Intent(context, GameActivity.class);
        context.startActivity(intent);
    }

    @Override
    public void startStats() {
        Intent intent = new Intent(context, StatsActivity.class);
        context.startActivity(intent);
    }

    @Override
    public void onLoadPoints() {
        repository.loadPoints(view, context);
    }


}

package com.example.englishcards.Stats;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.englishcards.R;

import androidx.appcompat.app.AppCompatActivity;

import static com.example.englishcards.Model.Data.correct;
import static com.example.englishcards.Model.Data.incorrect;

public class StatsActivity extends AppCompatActivity {

    private TextView Correct;
    private TextView Incorrect;
    private TextView Percent;
    private Button resetStatsBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stats_activity);
        Correct = findViewById(R.id.correct_points);
        Incorrect = findViewById(R.id.incorrect_points);
        Percent = findViewById(R.id.percent);
        resetStatsBtn = findViewById(R.id.reset_stats_btn);
        resetStatsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                correct = 0;
                incorrect = 0;
                RefreshStats();
            }
        });

        RefreshStats();
    }

    private void RefreshStats() {
        Correct.setText("Правильных ответов :"+String.valueOf(correct));
        Incorrect.setText("Неправильных ответов :"+String.valueOf(incorrect));
        try {
            Percent.setText(String.valueOf(Math.round((double) correct/(incorrect+ correct) * 100)) + " %");
        } catch (Exception e) {
            Percent.setText("0 %");
        }
    }
}

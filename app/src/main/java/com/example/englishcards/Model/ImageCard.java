package com.example.englishcards.Model;

import android.widget.ImageButton;

import com.squareup.picasso.Picasso;

public class ImageCard extends Card{

    private String url;

    public ImageCard(String rus, String eng, String url) {
        super(rus, eng);
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public void fillInterface(Object button) {
        Picasso.get().load(this.getUrl()).fit().into((ImageButton)button);
    }
}

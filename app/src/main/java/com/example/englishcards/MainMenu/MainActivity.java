package com.example.englishcards.MainMenu;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.englishcards.Game.GameActivity;
import com.example.englishcards.Model.Data;
import com.example.englishcards.Model.GameMode;
import com.example.englishcards.R;
import com.example.englishcards.Stats.StatsActivity;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements MainMenuContract.View {

    private Button RusEngButton;
    private Button EngRusButton;
    private Button ImageBtn;
    private Button Stats;

    private MainMenuContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        presenter = new MainMenuPresenter(this, this);
        presenter.onLoadPoints();

        RusEngButton =  findViewById(R.id.RusEngStart);
        RusEngButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.startGame(GameMode.RUS_ENG);
            }
        });
        EngRusButton =  findViewById(R.id.EngEusStart);
        EngRusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.startGame(GameMode.ENG_RUS);
            }
        });
        ImageBtn =  findViewById(R.id.ImageStart);
        ImageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.startGame(GameMode.IMAGE);
            }
        });

        Stats =  findViewById(R.id.stats);
        Stats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.startStats();
            }
        });
    }



}

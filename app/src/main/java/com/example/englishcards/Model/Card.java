package com.example.englishcards.Model;

public abstract class Card implements iFill{

    private String rus;
    private String eng;

    public Card (String rus, String eng){
        this.rus = rus;
        this.eng = eng;
    }

    public String getRus() {
        return rus;
    }

    public void setRus(String rus) {
        this.rus = rus;
    }

    public String getEng() {
        return eng;
    }

    public void setEng(String eng) {
        this.eng = eng;
    }



}

package com.example.englishcards.MainMenu;

import android.content.Context;
import android.content.SharedPreferences;


import static android.content.Context.MODE_PRIVATE;
import static com.example.englishcards.Model.Data.correct;
import static com.example.englishcards.Model.Data.incorrect;

public class MainMenuRepository implements MainMenuContract.Repository {

    public static SharedPreferences POINTS;
    public final static String SpCorrect = "SpCorrect";
    public final static String SpIncorrect = "SpIncorrect";

    @Override
    public void loadPoints(MainMenuContract.View view, Context context) {
        POINTS = context.getSharedPreferences("settings_key", MODE_PRIVATE);
        correct = POINTS.getInt(SpCorrect,0);
        incorrect = POINTS.getInt(SpIncorrect,0);
    }
}

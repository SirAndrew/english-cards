package com.example.englishcards.Game;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.englishcards.DatabaseHelper;
import com.example.englishcards.Model.Card;
import com.example.englishcards.Model.GameMode;
import com.example.englishcards.Model.ImageCard;
import com.example.englishcards.Model.SimpleCard;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.example.englishcards.Model.Data.MODE;

public class GameController {

    private DatabaseHelper databaseHelper;
    private SQLiteDatabase db;
    private Cursor cursor;
    private String Request;
    private ArrayList<Card> CardList;

    public ArrayList<Card> getCardList() {
        return CardList;
    }

    public GameController(Context applicationContext) {
        CardList = new ArrayList<>();

        databaseHelper = new DatabaseHelper(applicationContext);
        databaseHelper.create_db();
        // открываем подключение
        db = databaseHelper.open();
        if (MODE == GameMode.IMAGE)
            Request = "SELECT * FROM Card where url is not null";
        else
            Request = "SELECT * FROM Card";
        cursor = db.rawQuery(Request, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            String rus = cursor.getString(cursor.getColumnIndex("rus"));
            String eng = cursor.getString(cursor.getColumnIndex("eng"));
            String url = cursor.getString(cursor.getColumnIndex("url"));
            Card card = MODE == GameMode.IMAGE ? new ImageCard(rus, eng, url) : new SimpleCard(rus, eng);
            CardList.add(card);
            cursor.moveToNext();
        }
    }



    public ArrayList CreateQuestion () {
        Collections.shuffle(CardList);
        List<Card> q = CardList.stream().limit(4).collect(Collectors.<Card>toList());
        return (ArrayList) q;
    }
}

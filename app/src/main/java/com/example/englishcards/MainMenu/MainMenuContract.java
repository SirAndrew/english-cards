package com.example.englishcards.MainMenu;

import android.content.Context;

import com.example.englishcards.Model.GameMode;

public interface MainMenuContract {
    interface View {

    }

    interface Presenter {
        void onLoadPoints();
        void startGame(GameMode gameMode);
        void startStats();
    }

    interface Repository {
        void loadPoints(View v, Context c);
    }
}

package com.example.englishcards.Game;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.englishcards.Model.Card;
import com.example.englishcards.Model.GameMode;
import com.example.englishcards.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;

import androidx.appcompat.app.AppCompatActivity;

import static com.example.englishcards.Model.Data.MODE;
import static com.example.englishcards.Model.Data.correct;
import static com.example.englishcards.Model.Data.incorrect;
import static com.example.englishcards.MainMenu.MainMenuRepository.POINTS;
import static com.example.englishcards.MainMenu.MainMenuRepository.SpCorrect;
import static com.example.englishcards.MainMenu.MainMenuRepository.SpIncorrect;

public class GameActivity extends AppCompatActivity implements View.OnClickListener {

    private Button Answer1;
    private Button Answer2;
    private Button Answer3;
    private Button Answer4;

    private ImageButton ImageAnswer1;
    private ImageButton ImageAnswer2;
    private ImageButton ImageAnswer3;
    private ImageButton ImageAnswer4;

    private TextView questionTb;
    private TextView PointsTb;

    private GameController gameController;
    private ArrayList<Card> Cards;

    private ArrayList <ImageButton> imageButtons;
    private ArrayList <Button> buttons;
    private int answerNum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_activity);
        Answer1 = findViewById(R.id.answer1);
        Answer1.setOnClickListener(this);
        Answer2 = findViewById(R.id.answer2);
        Answer2.setOnClickListener(this);
        Answer3 = findViewById(R.id.answer3);
        Answer3.setOnClickListener(this);
        Answer4 = findViewById(R.id.answer4);
        Answer4.setOnClickListener(this);

        ImageAnswer1 = findViewById(R.id.ImgAnswer1);
        ImageAnswer1.setOnClickListener(this);
        ImageAnswer2 = findViewById(R.id.ImgAnswer2);
        ImageAnswer2.setOnClickListener(this);
        ImageAnswer3 = findViewById(R.id.ImgAnswer3);
        ImageAnswer3.setOnClickListener(this);
        ImageAnswer4 = findViewById(R.id.ImgAnswer4);
        ImageAnswer4.setOnClickListener(this);

        imageButtons= new ArrayList<>(Arrays.asList(ImageAnswer1, ImageAnswer2,ImageAnswer3,ImageAnswer4));
        buttons = new ArrayList<>(Arrays.asList(Answer1, Answer2,Answer3,Answer4));

        if(MODE==GameMode.IMAGE){
            for (ImageButton imgBtn: imageButtons) {
                imgBtn.setVisibility(View.VISIBLE);
            }
            for (Button btn: buttons) {
                btn.setVisibility(View.GONE);
            }

        }

        questionTb = findViewById(R.id.question);
        PointsTb = findViewById(R.id.points);

        Cards = new ArrayList<>();
        Context context = getApplicationContext();
        gameController = new GameController(context);
        RefreshTable();
    }

    private void RefreshTable () {
        Cards = gameController.CreateQuestion();
        int i = 0;
        if (MODE == GameMode.IMAGE) {
            for (Card card: Cards) {
                card.fillInterface(imageButtons.get(i));
                i++;
            }

        } else {
            for (Card card: Cards) {
                card.fillInterface(buttons.get(i));
                i++;
            }

        }
        ArrayList<Card> CardOrder = new ArrayList<>(Cards);
        Collections.shuffle(Cards);
        ArrayList<Card> QuestionCard = (ArrayList<Card>) Cards.stream().limit(1).collect(Collectors.<Card>toList());
        switch (MODE){
            case RUS_ENG:
                questionTb.setText(QuestionCard.get(0).getRus());
                break;
            case ENG_RUS:
                questionTb.setText(QuestionCard.get(0).getEng());
                break;
            case IMAGE:
                questionTb.setText(QuestionCard.get(0).getEng());
                break;
        }
        answerNum = CardOrder.indexOf(QuestionCard.get(0));

        PointsTb.setText(Integer.toString(correct-incorrect));
    }



    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.answer1:
            case R.id.ImgAnswer1:
                if (answerNum == 0) {
                    correct++;
                }
                else {
                    incorrect++;
                }
                break;
            case R.id.answer2:
            case R.id.ImgAnswer2:
                if (answerNum == 1) {
                    correct++;
                }
                else {
                    incorrect++;
                }
                break;
            case R.id.answer3:
            case R.id.ImgAnswer3:
                if (answerNum == 2) {
                    correct++;
                }
                else {
                    incorrect++;
                }
                break;
            case R.id.answer4:
            case R.id.ImgAnswer4:
                if (answerNum == 3) {
                    correct++;
                }
                else {
                    incorrect++;
                }
                break;
        }
        save();
        RefreshTable();
    }

    public  void save (){

        POINTS = getApplicationContext().getSharedPreferences("settings_key", MODE_PRIVATE);
        SharedPreferences.Editor ed = POINTS.edit();

        ed.putInt(SpCorrect, correct);
        ed.putInt(SpIncorrect, incorrect);
        ed.apply();

    }
}
